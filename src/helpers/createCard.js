const createCard = (number, layoutType) => {
	const card = document.createElement("div");

	if (layoutType === "double") {
		const front = document.createElement("div");
		const back = document.createElement("div");

		card.classList.add("card");
		card.style.zIndex = 1000 - number;
		card.dataset.cardnumber = number;
		front.classList.add("card-face");
		back.classList.add("card-face", "back");

		front.innerText = `Double: front of page ${number}`;
		back.innerText = `Double: back of page ${number}`;

		card.append(front, back);

		return card;
	} else if (layoutType === "single") {
		const front = document.createElement("div");

		card.classList.add("card");
		card.dataset.cardnumber = number;
		front.classList.add("card-face");

		front.innerText = `Single: front page of card ${number}`;

		card.append(front);

		return card;
	}
};
export default createCard;
