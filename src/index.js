import "normalize.css";

import "./css/styles.css";

import ready from "./helpers/ready";
import Flipbook from "./components/Flipbook";

const data = {
	title: "One Trillion Digits of Pi",
	get subtitle() {
		return `Volume ${this.thisVolumeNumber} of 1,000,000`;
	},
	volumesTotal: 1000000,
	thisVolumeNumber: 2,
	pages: 391,
	numberedpages: 386,
	label: currentCard => {
		if (currentCard === 1) {
			return "Volume cover";
		} else if (currentCard === 2) {
			return "ii—iii";
		} else if (currentCard === 196) {
			return "iv—v";
		}
		return (
			currentCard * 2 +
			this.thisVolumeNumber * this.numberedpages -
			this.numberedpages -
			2 +
			"—" +
			(currentCard * 2 - 1)
		);
	},
	size: {
		mm: {
			width: 110,
			height: 178,
			thickness: 24,
		},
		px: {
			width: 650,
			height: 1052,
		},
	},
	singlePageBreakpointInPixels: 600,
};

ready(Flipbook(data, document.getElementById("app")));
